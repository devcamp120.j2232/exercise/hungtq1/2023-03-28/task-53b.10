import com.devcamp.Author;
import com.devcamp.Book;
public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");

        Author author1 = new Author("Hung", 'm');

        Author author2 = new Author("Tran", 'l');

        System.out.println(author1);
        System.out.println(author2);
            //khoi tao book1 (không qty) va book2 (có qty)
        Book book1 = new Book ("Final Ballte", author1, 100000);
        Book book2 = new Book ("Legend", author2, 100000, 5);

        System.out.println(book1);
        System.out.println(book2);
    }
}
